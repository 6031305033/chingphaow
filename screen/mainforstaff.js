import 'react-native-gesture-handler';
import * as React from 'react';
import { NavigationContainer } from '@react-navigation/native';
import { SafeAreaView, StyleSheet, ScrollView, View, Text, StatusBar, } from 'react-native';
import Mapstaff from '../screen/mapforstaff';
import Homescreen from '../screen/home';
import UpdateScreen from '../screen/updatestatus';
import Staffreq from './stafflistreq.js';
import Icon from 'react-native-vector-icons/Ionicons';
import Icon2 from 'react-native-vector-icons/FontAwesome5';
import { createAppContainer,createSwitchNavigator } from 'react-navigation';
import { createStackNavigator } from 'react-navigation-stack';
import { createMaterialBottomTabNavigator } from 'react-navigation-material-bottom-tabs';
import ApiContainer from '../screen/ApiContainer';
import Mainscreen from './main';
import Loginscreen from './login';
export default class MainStaffScreen extends React.Component {
  static navigationOptions =
    {
      title: 'ชิงเผา Application',
      headerLeft:false,
      header:false
    };
  render() {
    return (

      <SafeAreaView style={{ flex: 1 }}>
        <AppContainer />
      </SafeAreaView>

    );
  }
}
const MainStack = createStackNavigator({
  Mainstaff : MainStaffScreen,
  Main : Mainscreen,
  Login : Loginscreen,
  Update : UpdateScreen
},
{
  defaultNavigationOptions: {
    header:false,
  }
  }
);
const TabNavigator = createMaterialBottomTabNavigator({
  Home: {
    screen: Homescreen,
    navigationOptions: {
      tabBarLabel: "ข่าวสาร",
      tabBarColor:'#0C7CA7',
      tabBarIcon: ({ tintColor }) => (
        <View>
          <Icon2 style={[{ color: tintColor }]} size={25} name={'newspaper'} />
        </View>
      )
    }
  },
  Map: {
    screen: Mapstaff,
    navigationOptions: {
      tabBarLabel: 'แผนที่',
      tabBarColor:'#0C7CA7',
      tabBarIcon: ({ tintColor }) => (
        <View>
          <Icon style={[{ color: tintColor }]} size={25} name={'ios-map'} />
        </View>
      )
    }
  },
  Reqlist: {
    screen: ApiContainer,
    navigationOptions: {
      tabBarLabel: "ลิสต์คำขอ",
      tabBarColor:'#0C7CA7',
      tabBarIcon: ({ tintColor }) => (
        <View>
          <Icon style={[{ color: tintColor }]} size={25} name={'ios-albums'} />
        </View>
      )
    }
  },
  Staffreq: {
    screen: Staffreq,
    navigationOptions: {
      tabBarLabel: "ลิสต์คำขอของฉัน",
      tabBarColor:'#0C7CA7',
      tabBarIcon: ({ tintColor }) => (
        <View>
          <Icon style={[{ color: tintColor }]} size={25} name={'ios-contact'} />
        </View>
      )
    }
  }
})
console.disableYellowBox = true;
const AppContainer = createAppContainer(
  createSwitchNavigator(
    {
      Tab: TabNavigator,
      Main: MainStack
    }
  )
);
