import React, { Component } from 'react';
import { StyleSheet, View, Text, TouchableOpacity, TextInput, Button, ActivityIndicator, Alert, Image, Dimensions, PermissionsAndroid } from 'react-native';
import MapView, { PROVIDER_GOOGLE } from 'react-native-maps';
import { Marker } from 'react-native-maps';
import Geolocation from 'react-native-geolocation-service';
import DatePicker from 'react-native-datepicker';
import { Title } from 'react-native-paper';
import { FlatList, ScrollView } from 'react-native-gesture-handler';
import Newscard from '../component/News';
import Newscard2 from '../component/News2';
let { width, height } = Dimensions.get('window');
const ASPECT_RATIO = width / height;
const LATITUDE = 20.050470250943587;
const LONGITUDE = 99.87799879855217;
const LATITUDE_DELTA = 0.0922;
const LONGITUDE_DELTA = LATITUDE_DELTA * ASPECT_RATIO;
export default class Homescreen extends Component {
  constructor(props) {
    super(props);
    this.state = {
      ready: false,
      error: null,
      loading: false,
      region: {
        latitude: LATITUDE,
        longitude: LONGITUDE,
        latitudeDelta: LATITUDE_DELTA,
        longitudeDelta: LONGITUDE_DELTA,
      },
      info: {
        name: null,
        temp: null,
        humidity: null,
        desc: null,
        wind: null,
        icon: null,
      }
    }
  }
   componentDidMount() {
    this.setState({ loading: true }, () => {
      Geolocation.getCurrentPosition(
        (position) => {
          this.setState({
            region: {
              latitude: position.coords.latitude,
              longitude: position.coords.longitude,
              latitudeDelta: LATITUDE_DELTA,
              longitudeDelta: LONGITUDE_DELTA,
            }
          });
          console.log(position);
         fetch("http://api.openweathermap.org/data/2.5/weather?lat="+this.state.region.latitude+"&lon="+this.state.region.longitude+"&appid=06221fc99afc08d9030d60c36b98c60e")
            .then(res => res.json())
            .then(data => {
              console.log(data)
              this.setState({
                info: {
                  name: data.name,
                  temp: Math.floor(data.main.temp - 273.15),
                  humidity: data.main.humidity,
                  wind: data.wind.speed,
                  desc: data.weather[0].description,
                  icon: data.weather[0].icon
                }
              })
            }).catch(err => {
              Alert.alert("Error" + err.message + "โปรดเช็คการเชื่อมต่อเน็ตของท่าน", [{ text: "OK" }])
            })
        }, 
          (error) => console.log(error.message),
        { enableHighAccuracy: false, timeout: 20000, forceRequestLocation: true },
      );
    });
    this.watchID = Geolocation.watchPosition(
      position => {
        this.setState({
          region: {
            latitude: position.coords.latitude,
            longitude: position.coords.longitude,
            latitudeDelta: LATITUDE_DELTA,
            longitudeDelta: LONGITUDE_DELTA,
          }
        });
      }
    );
  }
  componentWillUnmount() {
    Geolocation.clearWatch(this.watchID);
  }
  render() {
    return (
      <View style={styles.MainContainer}>
        <ScrollView>
          <Text style={styles.txtLogin}>Ching phaow</Text>
          <Text style={{ color: '#179BBE', textAlign: 'center' }}>สภาพอากาศ</Text>
          <View style={styles.ShowTop}>
            <Title style={{ justifyContent: 'center', marginTop: 10 }}>ที่อยู่ : {this.state.info.name}</Title>
            <Image style={{ width: 100, height: 50, justifyContent: 'center', position: 'relative' }}
              source={{ uri: 'http://openweathermap.org/img/w/' + this.state.info.icon + ".png" }}
            />
          </View>
          <View style={styles.Datacontainer1}>
            <Image source={require('../immg/Temp.png')}
              style={styles.inputicon1} />
            <Title>อุณหภูมิ : {this.state.info.temp} °C</Title>
          </View>
          <View style={styles.Datacontainer2}>
            <Image source={require('../immg/Water.png')}
              style={styles.inputicon2} />
            <Title>ความชื้นในอากาศ : {this.state.info.humidity} %</Title>
          </View>
          <View style={styles.Datacontainer1}>
            <Image source={require('../immg/Wind.png')}
              style={styles.inputicon1} />
            <Title>แรงลม : {this.state.info.wind} m/s</Title>
          </View>
          <View style={styles.Datacontainer2}>
            <Image source={require('../immg/State.png')}
              style={styles.inputicon2} />
            <Title>สถานะ : {this.state.info.desc}</Title>
          </View>
        </ScrollView>
      </View>
    );
  }
}

const styles = StyleSheet.create({
  MainContainer: {
    position: 'absolute',
    top: 0,
    left: 0,
    right: 0,
    bottom: 0,
    // alignItems: 'center',
    justifyContent: 'center',
    // backgroundColor: '#f5efdb'
  },

  inputicon1: {
    width: 50,
    height: 50,
    position: 'absolute',
    top: 15,
    left: 37,
    alignItems: 'center'
  },

  inputicon2: {
    width: 50,
    height: 50,
    position: 'absolute',
    top: 15,
    left: 240,
    alignItems: 'center'
  },

  ShowTop: {
    alignItems: 'center',
    marginTop: 15
  },

  Datacontainer1: {
    width: 300,
    height: 90,
    // borderWidth: 1,
    // borderColor: 'brown',
    paddingTop: 25,
    paddingBottom: 30,
    paddingRight: 15,
    marginTop: 20,
    marginLeft: 20,
    borderRadius: 25,
    alignItems: 'flex-end',
    backgroundColor: 'white',
    shadowColor: "#000",
    shadowOffset: {
      width: 0,
      height: 12,
    },
    shadowOpacity: 0.58,
    shadowRadius: 16.00,

    elevation: 24,
  },

  Datacontainer2: {
    width: 300,
    height: 90,
    // borderWidth: 1,
    // borderColor: 'brown',
    paddingTop: 25,
    paddingBottom: 30,
    paddingLeft: 15,
    marginTop: 20,
    marginLeft: 100,
    borderRadius: 25,
    backgroundColor: 'white',
    shadowColor: "#000",
    shadowOffset: {
      width: 0,
      height: 12,
    },
    shadowOpacity: 0.58,
    shadowRadius: 16.00,

    elevation: 24,
  },
  txtLogin: {
    fontWeight: "bold",
    fontSize: 20,
    textAlign: 'center'
  }
});  