import React, { Component, useState } from 'react';
import { Dimensions, StyleSheet, View, Text, TouchableOpacity, TextInput, Button, ActivityIndicator, Platform, PermissionsAndroid, ToastAndroid, SafeAreaView, DatePickerIOS, DatePickerAndroid } from 'react-native';
import MapView, { PROVIDER_GOOGLE } from 'react-native-maps';
import { Marker } from 'react-native-maps';
import Geolocation from 'react-native-geolocation-service';
import DateTimePicker from "react-native-modal-datetime-picker";
import { Title, Modal } from 'react-native-paper';
import moment from 'moment'
import { ScrollView } from 'react-native-gesture-handler';
import Icon from 'react-native-vector-icons/Ionicons';
let { width, height } = Dimensions.get('window');
const ASPECT_RATIO = width / height;
const LATITUDE = 20.050470250943587;
const LONGITUDE = 99.87799879855217;
const LATITUDE_DELTA = 0.0922;
const LONGITUDE_DELTA = LATITUDE_DELTA * ASPECT_RATIO;
export default class Addrequestscreen extends Component {
  constructor(props) {
    super(props);
    const { navigation } = this.props;
    const latitude = navigation.getParam('latitude', '');
    const longitude = navigation.getParam('longitude', '');
    this.state = {
      isLoading: true,
      page: 1,
      seed: 1,
      refreshing: false,
      region: {
        latitude: LATITUDE,
        longitude: LONGITUDE,
        latitudeDelta: LATITUDE_DELTA,
        longitudeDelta: LONGITUDE_DELTA,
      },
      isVisible: false,
      showMe: true,
      ready: false,
      where: { lat: null, lng: null },
      error: null,
      loading: false,
      info: {
        name: null,
        temp: null,
        humidity: null,
        desc: null,
        wind: null,
        icon: null,

      }
    }
  }
  handlePicker = (datetime) => {
    this.setState({
      isVisible: false,
      date: moment(datetime).format('YYYY/MM/DD HH:mm:SS')
    })
  }
  showPicker = () => {
    this.setState({
      isVisible: true
    })
  }
  hidePicker = () => {
    this.setState({
      isVisible: false
    })
  }
  // *Permission Allow*
  hasLocationPermission = async () => {
    if (Platform.OS === 'ios' ||
      (Platform.OS === 'android' && Platform.Version < 23)) {
      return true;
    }

    const hasPermission = await PermissionsAndroid.check(
      PermissionsAndroid.PERMISSIONS.ACCESS_FINE_LOCATION
    );

    if (hasPermission) return true;

    const status = await PermissionsAndroid.request(
      PermissionsAndroid.PERMISSIONS.ACCESS_FINE_LOCATION
    );

    if (status === PermissionsAndroid.RESULTS.GRANTED) return true;

    if (status === PermissionsAndroid.RESULTS.DENIED) {
      ToastAndroid.show('Location permission denied by user.', ToastAndroid.LONG);
    } else if (status === PermissionsAndroid.RESULTS.NEVER_ASK_AGAIN) {
      ToastAndroid.show('Location permission revoked by user.', ToastAndroid.LONG);
    }

    return false;
  }
  getLocation = async () => {
    const hasLocationPermission = await this.hasLocationPermission();

    if (!hasLocationPermission) return;

    this.setState({ loading: true }, () => {
      Geolocation.getCurrentPosition(
        (position) => {
          this.setState({
            where: { lat: position.coords.latitude, lng: position.coords.longitude },
            location: position,
            loading: false
          });
          console.log(position);
        },
        (error) => {
          this.setState({ location: error, loading: false });
          console.log(error);
        },
        { enableHighAccuracy: true, timeout: 15000, maximumAge: 10000, distanceFilter: 50, forceRequestLocation: true }
      );
    });
  }

  componentDidMount() {
    var date = new Date().getDate(); //Current Date
    var month = new Date().getMonth() + 1; //Current Month
    var year = new Date().getFullYear(); //Current Year
    var hours = new Date().getHours(); //Current Hours
    var min = new Date().getMinutes(); //Current Minutes
    var sec = new Date().getSeconds(); //Current Seconds
    this.setState({
      fromdate:
        year + '/' + month + '/' + date + ' ' + hours + ':' + min + ':' + sec,
    });
    Geolocation.getCurrentPosition(
      position => {
        this.setState({
          region: {
            latitude: position.coords.latitude,
            longitude: position.coords.longitude,
            latitudeDelta: LATITUDE_DELTA,
            longitudeDelta: LONGITUDE_DELTA,
          }
        });
      },
      (error) => console.log(error.message),
      { enableHighAccuracy: true, timeout: 20000, maximumAge: 1000 },
    );
    this.watchID = Geolocation.watchPosition(
      position => {
        this.setState({
          region: {
            latitude: position.coords.latitude,
            longitude: position.coords.longitude,
            latitudeDelta: LATITUDE_DELTA,
            longitudeDelta: LONGITUDE_DELTA,
          }
        });
      }
    );
  }
  componentWillUnmount() {
    Geolocation.clearWatch(this.watchID);
  }
  Refreshbutton() {
    this.setState({
      forceRefresh: Math.floor(Math.random() * 100)
    })
  }
  submitRequest = async () => {
    let myRequest = {
      name: this.state.name,
      phone: this.state.phone,
      address: this.state.address,
      latitude: this.state.region.latitude,
      longitude: this.state.region.longitude,
      fromdate: this.state.fromdate,
      todate: this.state.date,
      area: this.state.area,
      color: 'darkred',
      statusValue: 'กำลังรอเจ้าหน้าที่ตรวจสอบ',
      lastupdate: 'ยังไม่มีการอัพเดท'
    }

    alert(`คุณได้ส่งคำรองข้อเรียบร้อยแล้ว เจ้าหน้าที่จะแจ้งสถานะภายใน 1-5 วันก่อนวันดำเนินการ`);
    this.props.navigation.navigate('Map')
    fetch('https://chingphaow-application.herokuapp.com/requests/add', {
      method: 'POST',
      headers: {
        Accept: 'application/json',
        'Content-Type': 'application/json',
      },
      body: JSON.stringify(myRequest),
    }).then(console.log(myRequest))
  }
  _RenderloadingOverlay = () => {
    if (this.state.loading) {
      return (
        <View style={styles.loader}>
          <ActivityIndicator size="large" color="#0c9" />
          <Text>กำลังหาตำแหน่งของคุณ</Text>
        </View>
      );
    }
  };
  render() {
    return (
      <View style={styles.MainContainer}>
        <Text style={styles.txtLogin}>Ching phaow</Text>
        <Text style={{ color: '#179BBE' }}>ส่งคำขอ</Text>
        <ScrollView>
          <Text style={{ paddingLeft: 20, fontSize: 15, marginTop: 5, color: '#261a0d' }}>ชื่อจริง-นามสกุล:</Text>
          <View style={{ flexDirection: 'row' }}>
            <Icon style={{ marginTop: 15, marginLeft: 10 }} size={30} name={'md-person'} />
            <TextInput
              style={styles.textInputStyle}
              backgroundColor='white'
              paddingLeft={45}
              placeholder="ชื่อ-นามสกุล"
              placeholderTextColor='grey'
              onChangeText={(name) => this.setState({ name: name })}
            />
          </View>
          <Text style={{ paddingLeft: 20, fontSize: 15, marginTop: 5, color: '#261a0d' }}>เบอร์โทรติดต่อ:</Text>
          <View style={{ flexDirection: 'row' }}>
            <Icon style={{ marginTop: 15, marginLeft: 10 }} size={30} name={'ios-call'} />
            <TextInput
              style={styles.textInputStyle}
              backgroundColor='white'
              paddingLeft={45}
              keyboardType={'numeric'}
              placeholder="เบอร์โทร"
              placeholderTextColor='grey'
              onChangeText={(phone) => this.setState({ phone: phone })}
            />
          </View>

          <Text style={{ paddingLeft: 20, fontSize: 15, marginTop: 5, color: '#261a0d' }}>ที่อยู่ปัจจุบัน:</Text>
          <View style={{ flexDirection: 'row' }}>
            <Icon style={{ marginTop: 15, marginLeft: 10 }} size={30} name={'ios-home'} />
            <TextInput
              style={styles.textInputStyle}
              backgroundColor='white'
              paddingLeft={45}
              placeholder="ที่อยู่"
              placeholderTextColor='grey'
              onChangeText={(address) => this.setState({ address: address })}
            />
          </View>
          <Text style={{ paddingLeft: 20, fontSize: 15, marginTop: 5, color: '#261a0d' }}>ขนาดพื้นที่:</Text>
          <View style={{ flexDirection: 'row' }}>
            <Icon style={{ marginTop: 15, marginLeft: 10 }} size={30} name={'ios-contract'} />
            <TextInput
              style={styles.textInputStyle}
              backgroundColor='white'
              paddingLeft={45}
              keyboardType={'numeric'}
              placeholder="พื้นที่ (ตร.ม. โดยประมาณ)"
              placeholderTextColor='grey'
              onChangeText={(area) => this.setState({ area: area })}
            />
          </View>

          <Text style={{ textAlign: 'center', padding: 10 }}>กำหนดวันที่ดำเนินการ:</Text>
          <View style={{ flexDirection: 'row', alignSelf: 'center', marginRight: 40 }}>
            <Icon style={{ marginTop: 5, marginRight: 5 }} size={30} name={'ios-timer'} />
            <TouchableOpacity style={{ borderWidth: 1, width: 200, borderRadius: 5 }} onPress={this.showPicker}>
              <Text>{this.state.date}</Text>
            </TouchableOpacity>
          </View>
          <DateTimePicker
            isVisible={this.state.isVisible}

            mode="datetime" //The enum of date, datetime and time
            onConfirm={this.handlePicker}
            onCancel={this.hidePicker}
            // confirmBtnText="Confirm"
            // cancelBtnText="Cancel"
            onDateChange={(date) => { this.setState({ date: date }) }}
          />
          {this._RenderloadingOverlay()}
          <Text style={{ marginTop: 30, borderTopWidth: 1, paddingLeft: 20 }}>พิกัด บนแผนที่ :</Text>
          <View style={{ flexDirection: 'row' }}>
            <Icon style={{ marginTop: 15, marginLeft: 30 }} size={30} name={'ios-man'} />
            <Text style={{ marginTop: 10, marginLeft: 30 }}>ละติจูด : {this.state.region.latitude}</Text>
          </View>
          <Text style={{ marginBottom: 10, marginLeft: 75 }}>ลองติจูด : {this.state.region.longitude}</Text>
          <View style={{ width: 200, height: 40, marginLeft: 105 }}>
            <Button title='แสดงที่อยู่ปัจจุบันของฉัน' onPress={this.getLocation} />
          </View>
          <View style={{ marginTop: 10, width: 400, height: 200, alignSelf: 'center' }}>
            <MapView
              key={this.state.forceRefresh}
              style={styles.map}
              showsUserLocation={true}
              zoomEnabled={true}
              zoomControlEnabled={true}
              initialRegion={this.state.region}
              onRegionChange={region => this.setState({ region })}
              onRegionChangeComplete={region => this.setState({ region })}
            >
              <MapView.Marker
                draggable
                coordinate={this.state.region}
                onDragEnd={(e) => this.setState({ region: e.nativeEvent.coordinate })}
              />
            </MapView>
          </View>
          {/* <Button title='กำหนดจุดที่จะเผา' onPress={() => this.props.navigation.navigate('Sending')} /> */}
        </ScrollView>
        <View style={{ margin: 25, width: 200, height: 40, marginLeft: 25 }}>
          <Button
            title="ส่งข้อมูล"
            color="seagreen"
            onPress={() => {
              if (this.state.name == null || this.state.phone == null || this.state.address == null || this.state.date == null || this.state.area == null) {
                alert("กรุณากรอกข้อมูลให้ครบ")
                return;
              } 
              this.submitRequest()
            }
            }
          />

        </View>
      </View>


    );
  }
}

const styles = StyleSheet.create({
  MainContainer: {
    flex: 1,
    position: 'absolute',
    flex: 1,
    top: 0,
    left: 0,
    right: 0,
    bottom: 0,
    alignItems: 'center',
    justifyContent: 'flex-end',
    // backgroundColor:'#f5efdb'
  },
  map: {
    ...StyleSheet.absoluteFillObject,
  },
  body: {
    flex: 1,
  },
  map: {
    ...StyleSheet.absoluteFillObject,
  },
  body: {
    flex: 1,
  },
  welcome: {
    fontSize: 20,
    textAlign: 'center',
    margin: 10
  },
  textInputStyle: {
    height: 40,
    width: 350,
    marginLeft: 20,
    marginRight: 20,
    padding: 10,
    marginTop: 8,
    borderColor: 'black',
    borderBottomWidth: 1
  },
  txtLogin: {
    fontWeight: "bold",
    fontSize: 20
  },
  inicon: {
    width: 25,
    height: 25,
    position: 'absolute',
    top: 10,
    left: 37
  },
  modalView: {
    height: 200,
    justifyContent: 'center',
    alignItems: 'center',
    position: 'absolute'
  }

});  