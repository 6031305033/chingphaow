import React from 'react';
import { StyleSheet, Text, View, Button, TextInput, Alert, Picker, AsyncStorage, TouchableOpacity, Dimensions,PermissionsAndroid } from 'react-native';
import jwt_decode from 'jwt-decode'
import Geolocation from 'react-native-geolocation-service';
import Icon2 from 'react-native-vector-icons/MaterialIcons'
// let updatedate = new Date();
let { width, height } = Dimensions.get('window');
const ASPECT_RATIO = width / height;
const LATITUDE = 20.050470250943587;
const LONGITUDE = 99.87799879855217;
const LATITUDE_DELTA = 0.0922;
const LONGITUDE_DELTA = LATITUDE_DELTA * ASPECT_RATIO;
export default class UpdateScreen extends React.Component {
    constructor(props) {
        super(props)
        const { statusValue, color } = this.props.navigation.state.params
        const item = this.props.navigation.state.params.items
        this.state = {
            ustatusValue: statusValue,
            ucolor: color,
            uitem: item,
            staff_id: '',
            error: null,
            loading:false,
            region: {
                latitude: LATITUDE,
                longitude: LONGITUDE,
                latitudeDelta: LATITUDE_DELTA,
                longitudeDelta: LONGITUDE_DELTA,
            }


        }
    }
    getWeather  ()  {
        this.setState({ loading: true }, () => {
        Geolocation.getCurrentPosition(
            (position) => {
                this.setState({
                    region: {
                        latitude: position.coords.latitude,
                        longitude: position.coords.longitude,
                        latitudeDelta: LATITUDE_DELTA,
                        longitudeDelta: LONGITUDE_DELTA,
                    }
                });
                console.log(position);
                fetch("http://api.openweathermap.org/data/2.5/weather?lat="+this.state.region.latitude+"&lon="+this.state.region.longitude+"&appid=06221fc99afc08d9030d60c36b98c60e")
                    .then(res => res.json())
                    .then(data => {
                        console.log(data)
                        this.setState({
                            info: {
                                name: data.name,
                                temp: Math.floor(data.main.temp - 273.15),
                                humidity: data.main.humidity,
                                wind: data.wind.speed,
                                desc: data.weather[0].description,
                                icon: data.weather[0].icon
                            }
                        })
                    }).catch(err => {
                        Alert.alert("Error" + err.message + "โปรดเช็คการเชื่อมต่อเน็ตของท่าน", [{ text: "OK" }])
                    })
            },
            (error) => console.log(error.message),
            { enableHighAccuracy:false, timeout: 20000,forceRequestLocation: true },
        );
        });
        this.watchID = Geolocation.watchPosition(
            position => {
                this.setState({
                    region: {
                        latitude: position.coords.latitude,
                        longitude: position.coords.longitude,
                        latitudeDelta: LATITUDE_DELTA,
                        longitudeDelta: LONGITUDE_DELTA,
                    }
                });
            }
        );
    }
    componentWillUnmount() {
        Geolocation.clearWatch(this.watchID);
      }
    loadInitialState = async () => {
        const token = await AsyncStorage.getItem('usertoken');
        const decoded = jwt_decode(token)
        this.setState({
            staff_id: decoded.staff_id
        })
        console.log(decoded);
    }
    Updatestatus() {
        let myrequest = {
            id: this.props.navigation.state.params.id,
            staffid: this.state.staff_id,
            color: this.state.ucolor,
            statusValue: this.state.ustatusValue,
            lastupdate: this.state.updatedate
        }
        alert(`คุณได้อัพเดทสถานะเรียบร้อยแล้ว`);
        fetch('https://chingphaow-application.herokuapp.com/requests/update', {
            method: 'PUT',
            headers: {
                Accept: 'application/json',
                'Content-Type': 'application/json',
            },
            body: JSON.stringify(myrequest)
        }).then(console.log(myrequest))
        this.props.navigation.navigate('Staffreq')
    }
    componentDidMount() {
        this.getWeather()
        this.loadInitialState().done();
        var date = new Date().getDate(); //Current Date
        var month = new Date().getMonth() + 1; //Current Month
        var year = new Date().getFullYear(); //Current Year
        var hours = new Date().getHours(); //Current Hours
        var min = new Date().getMinutes(); //Current Minutes
        var sec = new Date().getSeconds(); //Current Seconds
        this.setState({
            updatedate:
                year + '/' + month + '/' + date + ' ' + hours + ':' + min + ':' + sec,
        });
    }
    render() {
        return (
            <View style={styles.container}>
                <View style={{ flexDirection: 'row' }}>
                    <TouchableOpacity style={{ marginLeft: 5 }} onPress={() => this.props.navigation.navigate('Reqlist')}>
                        <Icon2 name={'arrow-back'} size={30} />
                    </TouchableOpacity>
                    <Text style={styles.txtLogin}>Ching phaow</Text>
                </View>
                <Text style={{ color: '#179BBE', textAlign: 'center' }}>อัพเดทสถานะ</Text>
                <View>
                    <Text style={styles.txtbrowse}>สถานะ : </Text>
                    <View style={styles.Pickerbox1}>
                        <Picker
                            style={{ fontWeight: 'bold' }}
                            selectedValue={this.state.ustatusValue}
                            onValueChange={(itemValue, itemIndex) => this.setState({ ustatusValue: itemValue })} >

                            <Picker.Item label="กำลังรอเจ้าหน้าที่ตรวจสอบ" value="กำลังรอเจ้าหน้าที่ตรวจสอบ" color='red' />
                            <Picker.Item label="กำลังดำเนินการชิงเผา" value="กำลังดำเนินการชิงเผา" color='orange' />
                            <Picker.Item label="ชิงเผาเสร็จเรียบร้อยแล้ว" value="ชิงเผาเสร็จเรียบร้อยแล้ว" color='green' />

                        </Picker>
                    </View>
                    <Text style={styles.txtbrowse}>สีของสถานะ : </Text>
                    <View style={styles.Pickerbox2}>
                        <Picker
                            style={{ fontWeight: 'bold' }}
                            selectedValue={this.state.ucolor}
                            onValueChange={(itemValue, itemIndex) => this.setState({ ucolor: itemValue })} >

                            <Picker.Item label="สีแดง" value="darkred" color='red' />
                            <Picker.Item label="สีส้ม" value="darkorange" color='orange' />
                            <Picker.Item label="สีเขียว" value="seagreen" color='green' />

                        </Picker>
                    </View>
                </View>
                <View style={styles.Btn}>
                    <Button
                        title="อัพเดทสถานะ"
                        onPress={() => {
                            if (this.state.info.temp > 30 || this.state.info.wind > 1.8 || this.state.info.humidity < 75) {
                                alert("ไม่สามารถอัพเดทสถานะได้ในขณะนี้ กรุณาตรวจสอบอุณหภูมิ,ความแรงลมและความชื้นอีกครั้ง");
                                return;
                            }
                            else if (this.state.staff_id != this.props.navigation.state.params.staffid && this.props.navigation.state.params.staffid != null) {
                                alert("ขออภัย , จุดนี้มีเจ้าหน้าที่คนอื่นดูแลอยู่แล้ว")
                                return;
                            }
                            this.Updatestatus(this.state.item)
                        }
                        }

                    />
                </View>
            </View>
        );
    }
}

const styles = StyleSheet.create({
    container: {
        flex: 1,
        backgroundColor: '#fff',
    },
    txtbrowse: {
        paddingLeft: 25,
        marginBottom: 5,
        fontSize: 18,
        fontWeight: 'bold',
        marginTop: 20
        // color: '#261a0d'
    },
    Pickerbox1: {
        backgroundColor: 'white',
        marginBottom: 20,
        marginLeft: 20,
        marginRight: 20,
        shadowColor: "#000",
        shadowOffset: {
            width: 0,
            height: 12,
        },
        shadowOpacity: 0.58,
        shadowRadius: 16.00,
        elevation: 24,
    },
    Pickerbox2: {
        backgroundColor: 'white',
        marginBottom: 20,
        marginLeft: 20,
        marginRight: 20,
        shadowColor: "#000",
        shadowOffset: {
            width: 0,
            height: 12,
        },
        shadowOpacity: 0.58,
        shadowRadius: 16.00,
        elevation: 24,
    },
    Btn: {
        marginLeft: 20,
        marginRight: 20,
        borderRadius: 25
    },
    Toptxt: {
        marginBottom: 40,
        fontSize: 25,
        fontWeight: 'bold'

    },
    txtLogin: {
        fontWeight: "bold",
        fontSize: 20,
        textAlign: 'center',
        marginLeft: 110
    }
});

// if(this.state.info.temp > 35||this.state.info.wind > 1.6||this.state.info.humidity < 65){