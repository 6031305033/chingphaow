import React from 'react';
import { Text, View, FlatList, Image, Button, Picker, Modal, TouchableOpacity, StyleSheet, Linking, TouchableHighlightBase, SafeAreaView,H1 } from 'react-native';
import Icon from 'react-native-vector-icons/AntDesign'
import Icon1 from 'react-native-vector-icons/Ionicons'
import { Badge, withBadge } from 'react-native-elements'
import TimeAgo from 'react-native-timeago';
import { TouchableHighlight } from 'react-native-gesture-handler';
import MapView, { PROVIDER_GOOGLE } from 'react-native-maps';

const BadgedIcon = withBadge(1)(Icon)
let Newscard = (props) => {
    return (
        <View style={styles.MainContainer}>
            <Text style={{fontSize:20,fontWeight:'bold'}}>EP.1“ชิงเผา” ก่อนวันอันตราย และทางเลือกคืนลมหายใจบริสุทธิ์ให้เชียงใหม่</Text>
            <Image style={{width:380,height:200,marginTop:10,marginBottom:10}}source={{ uri: 'https://s3-ap-southeast-1.amazonaws.com/thaipbs-citizen/wp-content/uploads/2019/02/17105533/DSC_0156_resize.jpg' }} />
            <Text>นับเป็นเวลาสิบกว่าปีมาแล้วที่จังหวัดเชียงใหม่และภูมิภาคเหนือบนประสบปัญหาวิกฤติหมอกควันซึ่งทำให้เกิดฝุ่นละอองขนาดเล็ก PM 2.5  ซึ่งองค์การอนามัยโลกกำหนดไว้ว่าเป็นค่าที่เป็นอันตรายร้ายแรงต่อสุขภาพ และคนในภูมิภาคนี้ประสบกับภาวะเจ็บป่วยกับโรคทางเดินหายใจและโรคอื่นๆ อย่างหนักในทุกๆ ปีเมื่อเข้าหน้าแล้ง
                สำหรับมลพิษทางอากาศ PM2.5 นี้  เป็นฝุ่นที่มีเส้นผ่าศูนย์กลางไม่เกิน 2.5 ไมครอน เกิดจากทั้งการเผาไหม้เครื่องยนต์พาหนะ การเผาวัสดุเศษพืชทางการเกษตร
            </Text>
        </View>
    );
}
export default Newscard;
const styles = StyleSheet.create({
    InfoCard: {
        marginTop: 10,
        flex: 1,
        // borderWidth: 1, 
        borderRadius: 10,
        backgroundColor: 'white',
        shadowColor: "#000",
        shadowOffset: {
            width: 0,
            height: 12,
        },
        shadowOpacity: 0.58,
        shadowRadius: 16.00,

        elevation: 24,
    },
    Info: {
        flex: 1,
        flexDirection: 'row',
        paddingLeft: 10,
        marginTop: 10,
        marginBottom: 5
    },
    MainContainer: {
        flex: 1,
        justifyContent: 'center',
        alignItems: 'center',
        backgroundColor: '#F5FCFF',
        borderRadius:10,
        borderWidth:1,
        padding:10
    }

});
