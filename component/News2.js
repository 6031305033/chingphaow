import React from 'react';
import { Text, View, FlatList, Image, Button, Picker, Modal, TouchableOpacity, StyleSheet, Linking, TouchableHighlightBase, SafeAreaView,H1 } from 'react-native';
import Icon from 'react-native-vector-icons/AntDesign'
import Icon1 from 'react-native-vector-icons/Ionicons'
import { Badge, withBadge } from 'react-native-elements'
import TimeAgo from 'react-native-timeago';
import { TouchableHighlight } from 'react-native-gesture-handler';
import MapView, { PROVIDER_GOOGLE } from 'react-native-maps';

const BadgedIcon = withBadge(1)(Icon)
let Newscard2 = (props) => {
    return (
        <View style={styles.MainContainer}>
            <Text style={{fontSize:20,fontWeight:'bold'}}>ฝุ่น PM 2.5 ภาคเหนือตอนบนยังวิกฤต</Text>
            <Image style={{width:380,height:200,marginTop:10,marginBottom:10}}source={{ uri: 'https://static.posttoday.com/media/content/2019/04/07/E7C0EBDE11F54D9F904E18596593A346.jpg' }} />
            <Text>กรมควบคุมมลพิษ (คพ.) รายงาน สถานการณ์ฝุ่น PM 2.5 บริเวณภาคเหนือ พบ 12 จังหวัด มีปริมาณฝุ่น PM 2.5 
                เกินค่ามาตรฐาน วัดได้ระหว่าง 52-190 มคก./ลบ.ม. พื้นที่ที่มีฝุ่น PM 2.5 มากที่สุดคือ ต.เวียงพางคำ 
                อ.แม่สาย จ.เชียงราย และ ต.เมืองคอง อ.เชียงดาว จ.เชียงใหม่ 190 มคก./ลบ.ม. รองลงมาคือ ต.จองคำ อ.เมือง 
                จ.แม่ฮ่องสอน 121 มคก./ลบ.ม.
            </Text>
        </View>
    );
}
export default Newscard2;
const styles = StyleSheet.create({
    InfoCard: {
        marginTop: 10,
        flex: 1,
        // borderWidth: 1, 
        borderRadius: 10,
        backgroundColor: 'white',
        shadowColor: "#000",
        shadowOffset: {
            width: 0,
            height: 12,
        },
        shadowOpacity: 0.58,
        shadowRadius: 16.00,

        elevation: 24,
    },
    Info: {
        flex: 1,
        flexDirection: 'row',
        paddingLeft: 10,
        marginTop: 10,
        marginBottom: 5
    },
    MainContainer: {
        flex: 1,
        justifyContent: 'center',
        alignItems: 'center',
        backgroundColor: '#F5FCFF',
        borderRadius:10,
        borderWidth:1,
        padding:10,
        marginTop:10
    }

});
