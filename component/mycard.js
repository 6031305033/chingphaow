import React from 'react';
import { Text, View, FlatList, Image, Button, Picker, Modal, TouchableOpacity, StyleSheet, Linking, TouchableHighlightBase, SafeAreaView } from 'react-native';
import Icon from 'react-native-vector-icons/AntDesign'
import Icon1 from 'react-native-vector-icons/Ionicons'
import { Badge, withBadge } from 'react-native-elements'
import TimeAgo from 'react-native-timeago';
import { TouchableHighlight } from 'react-native-gesture-handler';
import MapView, { PROVIDER_GOOGLE } from 'react-native-maps';

const BadgedIcon = withBadge(1)(Icon)
let Mycard = (props) => {
    //let { id, firstname, lastname, email, phone, photo } = props.items;
    let { staffid, first_name, last_name, name, phone, address, latitude, longitude, fromdate, todate, statusValue, color, lastupdate, area } = props.items;
    return (
            <View style={{backgroundColor:color,borderRadius:10,marginTop:10}}>
                <View style={{ alignSelf: 'flex-end', position: 'absolute', marginTop: 10, flexDirection: 'row'}}>
                    <Text style={{ fontSize: 12,color:'white'}}>สถานะ :</Text>
                    <Text style={{ fontSize: 14, fontWeight: 'bold',color:'white' }}>{statusValue}</Text>
                </View>
                <View style={styles.Info}>
                    <Text style={{ fontSize: 16, fontWeight: 'bold',color:'white' }}>ชื่อผู้ส่ง : </Text>
                    <Text style={{ fontSize: 16 ,color:'white'}}>{name}</Text>
                </View>
                <View style={styles.Info}>
                    <Text style={{ fontSize: 13, fontWeight: 'bold',color:'white' }}>วันที่ดำเนินการ : </Text>
                    <Text style={styles.RequestInfo}>{todate}</Text>
                </View>
                <View style={{ marginTop: 10, width: 380, height: 200, alignSelf: 'center' }}>
                    <MapView
                        provider={PROVIDER_GOOGLE}
                        style={styles.map}
                        showsUserLocation={true}
                        zoomEnabled={true}
                        zoomControlEnabled={true}
                        scrollEnabled={false}
                        rotateEnabled={false}
                        initialRegion={{
                            latitude: 20.050470250943587,
                            longitude: 99.87799879855217,
                            latitudeDelta: 0.2922,
                            longitudeDelta: 0.0421,
                        }}>
                        <MapView.Marker
                            pinColor={color}
                            coordinate={{
                                latitude: latitude,
                                longitude: longitude
                            }}
                        ></MapView.Marker>
                    </MapView>
                </View>
                <View style={styles.Info}>
                    <Text style={{ fontSize: 16, fontWeight: 'bold',color:'white' }}>ที่อยู่ : </Text>
                    <Text style={styles.RequestInfo}>{address}</Text>
                </View>
                <View style={styles.Info}>
                    <Text style={{ fontSize: 16, fontWeight: 'bold',color:'white' }}>พื้นที่(โดยประมาณ) : </Text>
                    <Text style={styles.RequestInfo}>{area} ตร.ม.  |  {area / 1600} ไร่</Text>
                </View>
                <View style={styles.Info}>
                    <Text style={{ fontSize: 16, fontWeight: 'bold',color:'white' }}>ผู้ดูแล : </Text>
                    <Text style={{ fontSize:16,color: 'black' }}>{first_name} {last_name}</Text>
                </View>
                <View style={styles.Info}>
                    <Text style={{ fontSize: 16, fontWeight: 'bold',color:'white' }}>อัพเดทล่าสุด : </Text>
                    <Text style={styles.RequestInfo}>{lastupdate}  |  </Text>
                    <TimeAgo style={styles.RequestInfo} time={lastupdate} />
                </View>
                <View style={{ alignItems: 'center', flexDirection: 'row' }}>
                    <Image style={styles.Logo} source={require('../immg/Editlogo.png')} />
                    <Text style={{ fontSize: 16, fontWeight: 'bold', color: 'black', marginRight: 10, marginLeft: 5 }}>แตะค้างเพื่ออัพเดทสถานะ</Text>
                    <Image style={styles.Logo1} source={require('../immg/navi.png')} />
                    <Text style={{ fontSize: 16, fontWeight: 'bold', color: 'black', marginRight: 10, marginLeft: 5 }} onPress={() => Linking.openURL(`https://www.google.com/maps?ie=UTF8&z=13&q=${latitude},${longitude}`)}>ขอเส้นทาง</Text>
                </View>
            </View>
    );
}
export default Mycard;
const styles = StyleSheet.create({
    InfoCard: {
        marginTop: 10,
        flex: 1,
        // borderWidth: 1, 
        borderRadius: 10,
        backgroundColor: 'white',
        shadowColor: "#000",
        shadowOffset: {
            width: 0,
            height: 12,
        },
        shadowOpacity: 0.58,
        shadowRadius: 16.00,

        elevation: 24,
    },
    Info: {
        flex: 1,
        flexDirection: 'row',
        paddingLeft: 10,
        marginTop: 10,
        marginBottom: 5
    },
    RequestInfo: {
        fontSize: 13,
        color:'white'
    },
    Logo: {
        height: 30,
        width: 30,
        marginBottom: 10,
        flex: 0.5,
        flexDirection: 'column'
    },
    Logo1: {
        height: 40,
        width: 40,
        alignSelf: 'center'
    },
    MainContainer: {
        flex: 1,
        justifyContent: 'center',
        alignItems: 'center',
        backgroundColor: '#F5FCFF',
    },
    map: {
        ...StyleSheet.absoluteFillObject,
    }

});
